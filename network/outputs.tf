output "vpc" {
    value = aws_vpc.main
}

output "subnet_private" {
    value = aws_subnet.private
}

output "subnet_public" {
    value = aws_subnet.public
}

output "route53" {
    value = aws_route53_zone.main
}