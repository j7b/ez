resource "aws_vpc" "main" {
    cidr_block          = var.vpc_cidr_block
    instance_tenancy    = "default"

    tags = {
        Name = "${var.this_name} vpc"
    }
}

resource "aws_subnet" "private" {
    count = length(var.availability_zones)

    vpc_id              = aws_vpc.main.id
    availability_zone   = sort(var.availability_zones)[count.index]
    cidr_block          = "10.${var.subnet}.${lookup(local.count_to_private_subnet, count.index)}.0/24"
    map_public_ip_on_launch = false

    tags = {
        Name = "${var.this_name} private ${count.index}"
    }
}

resource "aws_subnet" "public" {
    count = length(var.availability_zones)

    vpc_id              = aws_vpc.main.id
    availability_zone   = sort(var.availability_zones)[count.index]
    cidr_block          = "10.${var.subnet}.${lookup(local.count_to_public_subnet, count.index)}.0/24"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.this_name} public ${count.index}"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.main.id

    tags = {
        Name = "${var.this_name} IGW"
    }
}

resource "aws_nat_gateway" "ngw" {
    allocation_id   = aws_eip.ngw_eip.id
    subnet_id       = aws_subnet.public[0].id

    tags = {
        Name = "${var.this_name} NGW"
    }
}

resource "aws_eip" "ngw_eip" {
    vpc = true

    tags = {
        Name = "${var.this_name} NGW EIP"
    }
}