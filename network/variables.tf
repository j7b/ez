# Family; company name
variable "family" {
    type    = string
}

# Used for naming things in the env
variable "this_name" {
    type    = string
}

# Region.
variable region {
    type    = string
    default = "ap-southeast-2"
}

# AZs.
variable "availability_zones" {
    type    = list(string)
    default = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
}

# CIDR Block for VPC
variable "vpc_cidr_block" {
    type    = string
    default = "10.0.0.0/16"
}

# Subnet for VPC - based on above basically
variable "subnet" {
    type    = string
    default = "0"
}

# Domain for things
variable "domain" {
    type    = string
}

variable "instance_count" {
    type    = number
    default = 1
}

locals {
    # Simple hack for subnetting
    count_to_private_subnet = {
        "0" = "0"
        "1" = "8"
        "2" = "16"
    }
    count_to_public_subnet = {
        "0" = "64"
        "1" = "72"
        "2" = "80"
    }
}