data "aws_ami" "aml2" {
    most_recent = true

    filter {
        name   = "owner-alias"
        values = ["amazon"]
    }

    filter {
        name   = "name"
        values = ["amzn2-ami-hvm*"]
    }

    filter {
        name   = "architecture"
        values = ["x86_64"]
    }
}

resource "aws_instance" "web" {
    count = var.instance_count
    ami           = data.aws_ami.aml2.id
    instance_type = "t3.micro"

    tags = {
        Name = "${var.family}-${var.this_name}-${format("%02d", count.index + 1)}"
    }
}

resource "aws_launch_template" "app" {
    name_prefix   = "${var.family}-${var.this_name}"
    image_id      = data.aws_ami.aml2.id
    instance_type = "c5.large"
}

resource "aws_autoscaling_group" "app" {
    availability_zones = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
    desired_capacity   = 1
    max_size           = 1
    min_size           = 1

    launch_template {
        id      = aws_launch_template.app.id
        version = "$Latest"
    }
}