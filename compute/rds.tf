resource "aws_db_instance" "main" {
    allocated_storage   = 10
    engine              = "mysql"
    instance_class      = "db.t2.micro"
    name                = "${var.this_name}-rds"
    username            = "${var.RDS_USER}"
    password            = "${var.RDS_PASS}"

}