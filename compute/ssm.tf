resource "aws_kms_key" "rds_creds" {
  description             = "${lower(var.this_name)}-rds-creds"
}

resource "aws_ssm_parameter" "rds_host" {
  name  = "${lower(var.this_name)}-rds-host"
  type  = "String"
  value = "${aws_db_instance.main.endpoint}"
}

resource "aws_ssm_parameter" "rds_user" {
  name  = "${lower(var.this_name)}-rds-user"
  type  = "String"
  value = "${var.RDS_USER}"
}

resource "aws_ssm_parameter" "rds_pass" {
  name    = "${lower(var.this_name)}-rds-pass"
  type    = "SecureString"
  value   = "${var.RDS_PASS}"
  key_id  = aws_kms_key.rds_creds.key_id
}

