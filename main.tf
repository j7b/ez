module "network" {
    source = "./network"
    family = "NM"
    this_name = "Test"
    domain = "nmt.j7b.net"
    instance_count = 1
}