terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.21.0"
    }
  }
}

provider "aws" {
  region  = "ap-southeast-2"
  default_tags {
    tags = {
      Environment = "nearmap-test"
    }
  }
}